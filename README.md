AWS Best Practices Audit
============================
This composite will monitor supported AWS services and report on things CloudCoreo developers think are violations of best practices


## Description
This ruleset will list all inventory for cloud objects and endpoint

## Hierarchy



## Required variables with no default

**None**


## Required variables with default

**None**


## Optional variables with default

**None**


## Optional variables with no default

**None**

## Tags
1. Audit
1. Best Practices
1. AWS


## Categories


## Diagram


## Icon


